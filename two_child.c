#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>

const int BUFFER_SIZE = 100;

void get_current_time(char* into_string)
{
    time_t cur_time;
    time(&cur_time);
    struct tm* cur_tm = localtime(&cur_time);
    struct timespec cur_timespec;
    clock_gettime(CLOCK_REALTIME, &cur_timespec);
    long milliseconds = cur_timespec.tv_nsec / 1000000;

   snprintf(into_string, BUFFER_SIZE, "%d:%d:%d:%ld", cur_tm->tm_hour, cur_tm->tm_min, cur_tm->tm_sec, milliseconds);
}

void print_pid_info(pid_t pid)
{
    char time_string[BUFFER_SIZE];
    if (pid == 0)
    {
        get_current_time(time_string);
        printf("Child process id=%d, time: %s\n ", getpid(), time_string);
        get_current_time(time_string);
        printf("this child's parent process id=%d, time: %s\n", getppid(), time_string);
    }
    else if (pid > 0)
    {
        get_current_time(time_string);
        printf("Parent process id=%d time: %s\n", getpid(), time_string);
    }
    else
    {
        printf("Can't fork new process\n");
    }
}

int main()
{
    pid_t pid1 = fork();
    print_pid_info(pid1);

    if (pid1 > 0)
    {
        pid_t pid2 = fork();
        print_pid_info(pid2);
        if (pid2 > 0)
        {
            system("ps -x");
        }
    }

    return 0;
}
