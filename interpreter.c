#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdbool.h>


const int INPUT_STRING_MAX_SIZE = 200;
const int COMMAND_ARG_MAX_SIZE = 100;

void split(char* string, char** into_strings)
{
    char* delimiter = "\n";
    char* part = strtok(string, delimiter);
    int i;
    for (i = 0; part != NULL; i++)
    {
        into_strings[i] = part;
        part = strtok(NULL, delimiter);
    }
    into_strings[i] = NULL;
}

bool handle_command(char** argv)
{
    pid_t pid = fork();
    if (pid == 0)
    {
        pid_t inner_pid = fork();
        if (inner_pid == 0)
        {
            execvp(argv[0], argv);
            printf("Command not found: %s\n", argv[0]);
        }
        else if (inner_pid > 0)
        {
            int status;
            waitpid(inner_pid, &status, 0);
            if (WIFEXITED(status))
            {
                int exit_status = WEXITSTATUS(status);
                if (exit_status != 0)
                {
                    printf("Exit with code %d\n", exit_status);
                }
            }
            else
            {
                printf("Executing interrupted\n");
            }
        }
        else if (inner_pid < 0)
        {
            printf("Can't fork new process\n");
        }
        return false;
    }
    else if (pid < 0)
    {
        printf("Can't fork new process\n");
    }
    return true;
}

int main()
{
    bool should_continue = true;
    while (should_continue)
    {
        char command[INPUT_STRING_MAX_SIZE];
        fgets(command, INPUT_STRING_MAX_SIZE, stdin);
        char* argv[COMMAND_ARG_MAX_SIZE];
        split(command, argv);
        should_continue = handle_command(argv);
    }
    return 0;
}
